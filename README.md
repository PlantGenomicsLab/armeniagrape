# Armenia Grape Project

Population genetics on a variety of grape plants from Armenia. 

*Goal:* To define population structure in the Armenian grape population through RADseq and potential SSR data. 

NCBI Bioproject: PRJNA464034

* [X]  Demultiplex and QC data
* [X]  Initial PyRAD and STRUCTURE runs
* [ ]  Identify samples and variants for population structure
* [ ]  Finalize pyrad and structure-based population
* [ ]  Generate figures


![Inline image](GRAPE_MAP.png) 