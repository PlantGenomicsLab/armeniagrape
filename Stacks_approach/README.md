# Armenia Grape Project - using Stacks

This approach was not determined to be successful due to pooling effects. Stacks was run both wit and without the reference genome.

Samples are demultiplexed using the stacks program process_radtags on the two pools:

```
/isg/shared/apps/stacks/1.44/bin/process_radtags \
-1 ../trimmed_data/Pool1_trimmed_1.fastq.gz \
-2 ../trimmed_data/Pool1_trimmed_2.fastq.gz \
-b barcodes_Pool1 \
-o demultiplex_output/ \
-y fastq \
-i gzfastq \
--inline_null \
-e sbfI \
-P \
-c \
-q \
-r \
--barcode_dist_1 1 \
--adapter_1 GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT \
--adapter_2 AGATCGGAAGAGCGAGAACAA
```

Results :

Pool 1

```
76380632 total sequences
  405561 reads contained adapter sequence (0.5%)
 9262756 ambiguous barcode drops (12.1%)
       0 low quality read drops (0.0%)
 8887318 ambiguous RAD-Tag drops (11.6%)
57824997 retained reads (75.7%)
```

Pool 2
```
103119506 total sequences
  2495230 reads contained adapter sequence (2.4%)
 15293984 ambiguous barcode drops (14.8%)
        0 low quality read drops (0.0%)
 15896841 ambiguous RAD-Tag drops (15.4%)
 69433451 retained reads (67.3%)
```

# Genome ref map

1.  Align reads to *Vitis vinifera* genome
``` 
bwa mem -t 16 /UCHC/LABS/Wegrzyn/Grapes/data/ref_map/genome/Vvinifera_145_Genoscope.12X \
/UCHC/LABS/Wegrzyn/Grapes/data/process_radtags/demultiplex_output/Pool1_BC021.1.fq \
/UCHC/LABS/Wegrzyn/Grapes/data/process_radtags/demultiplex_output/Pool1_BC021.2.fq > Pool1_BC021.sam
```

2.  Run ref_map.pl on combined pool samples with > 1,000,000 reads
```
/isg/shared/apps/stacks/1.44/bin/ref_map.pl \
-s /UCHC/LABS/Wegrzyn/Grapes/data/ref_map/trial/Pool1_BC01.sam \
-s /UCHC/LABS/Wegrzyn/Grapes/data/ref_map/trial/Pool1_BC02.sam \
...
-o . \
-b 1 \
-S
```

3.  Run populations
```
populations -P /UCHC/LABS/Wegrzyn/Grapes/data/ref_map/trial/population_trial/stack_output \
-O population_output_r93/ -r 93 --structure
```

4.  Run STRUCTURE
k=2, 4, 6, 8, 10, 12, 14, 16, 18, 20 and 54
number of loci=6654

We use the program pophelper to plot STRUCTURE outputs. We also experiment with different R values (percent of samples a loci must be present in). R = 4 means 3 samples in a 54 sample pool. Results when combined: 

![R = 4, samples with > 1e6 reads](refmap_R04_filtered.png)

Pooling is evident as it will be without using the reference genome. 

# Genome denovo map

Denovo map runs in a similar manner to refmap but without alignment.

```
/isg/shared/apps/stacks/1.44/bin/denovo_map.pl \
-s ../process_radtags/demultiplex_output/Pool1_BC01.fq \
-s ../process_radtags/demultiplex_output/Pool1_BC02.fq \
...
-o . \
-b 1 \
-S \
-X "ustacks:-M 3" \
-X "ustacks:-m 2" \
-X "ustacks:-R" \
-X "ustacks:--deleverage"
```

Populations is then run on the output followed by STRUCTURE. Denovo populations show an increased amount of homogeneity between samples and far less population structure than refmap. This makes sense given variants the reference genome would add.

![R = 4, pool 1](denovomap_pool1_all.png)
![R = 4, pool 2](denovomap_pool2_all.png)

When samples are combined during denovo map so variants across all pools are included and filtered to R = 4 and >1e6 reads samples, pooling effects are still evident:

![R = 4, all samples](denovomap_pool12_filtered.PNG)

Therefore, we decided not to use stacks for this population. 